package Pages1.GreenValley1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private WebDriver driver;
	@FindBy(id="userid")
	private WebElement userid;
	
	@FindBy(id="password")
	private WebElement password;
	
	@FindBy(id="loginbutton")
	private WebElement login;
	
	LoginPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
   public void login(String username,String password)
   {
    userid.sendKeys(username);	   
   }
}

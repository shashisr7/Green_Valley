package Pages1.GreenValley1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class ChatRoom {
	   private WebDriver driver;
		@FindBy(id="textarea1")
		private WebElement testarea1;
		
		@FindBy(id="textarea2")
		private WebElement textarea2;
		
		@FindBy(id="searchButton")
		private WebElement searhButton;
		
		ChatRoom(WebDriver driver)
		{
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}
		
	   public void viewMessages()
	   {
		  String messages= testarea1.getAttribute("text");
		   System.out.println("Messages "+messages);
	   }
}

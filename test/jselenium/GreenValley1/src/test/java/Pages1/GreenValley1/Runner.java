package Pages1.GreenValley1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Runner
{
  private WebDriver driver;
	
	@BeforeMethod
	public void preSteps()
	{
		driver=new FirefoxDriver(); 
		driver.get("url");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	@AfterMethod
	public void postSteps()
	{
		driver.close();
	}
	
	
	@Test(dataProvider="fetchUserData")
	public void Login(String username,String password,String chatroom,String message)
	{
		LoginPage lp=new LoginPage(driver);
		lp.login(username, password);
	}
	
	@Test
	public void ClickChatRoom(String username,String passowrd,String chatroom,String message)
	{
		
		HomePage hm=new HomePage(driver);
		hm.click(chatroom);
	}
	
	@Test
	public void ViewMessages(String username,String Password,String chatroom,String message)
	{
		ChatRoom cr=new ChatRoom(driver);
		cr.viewMessages();
	}
	
	@DataProvider(name="fetchUserData")
	public static Object[][] fetchUserData()
	{
		return new Object[][] {{"User_1","pwd1","chat1","message1"},{"User_2","pwd2","chat1","message2"}};
	}
	

		
		
		
	}
	
	
	
	
	
	


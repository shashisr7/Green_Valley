package org.green.valley.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.green.valley.database.DatabaseClass;
import org.green.valley.model.Group;

public class GroupService {

	private Map<String, Group> groups = DatabaseClass.getGroups();
	
	public GroupService() {
		groups.put("green", new Group(1L, "green", "green", "valley"));
	}
	
	public List<Group> getAllProfiles() {
		return new ArrayList<Group>(groups.values()); 
	}
	
	public Group getProfile(String profileName) {
		return groups.get(profileName);
	}
	
	public Group addProfile(Group profile) {
		profile.setId(groups.size() + 1);
		groups.put(profile.getProfileName(), profile);
		return profile;
	}
	
	public Group updateProfile(Group profile) {
		if (profile.getProfileName().isEmpty()) {
			return null;
		}
		groups.put(profile.getProfileName(), profile);
		return profile;
	}
	
	public Group removeProfile(String profileName) {
		return groups.remove(profileName);
	}
	
	
}

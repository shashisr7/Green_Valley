package org.green.valley.database;

import java.util.HashMap;
import java.util.Map;

import org.green.valley.model.Message;
import org.green.valley.model.Group;

public class DatabaseClass {

	private static Map<Long, Message> messages = new HashMap<>();
	private static Map<String, Group> groups = new HashMap<>();

	
	public static Map<Long, Message> getMessages() {
		return messages;
	}
	
	public static Map<String, Group> getGroups() {
		return groups;
	}

	
	
	
}

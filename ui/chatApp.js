
	var chatApp = angular.module('chatModule', ['ngRoute']);


	chatApp.config(['$routeProvider', function($routeProvider) {
		$routeProvider
		.when('/dev/:gid', {
				templateUrl : 'chatInterface.html',
				controller: 'chatController',
			})
		.when('/chat', {
				templateUrl : 'chatDetails.html',
			})
	}]);

	//Main Controller
	chatApp.controller('chatController',function($scope, $routeParams, $http, chatService){
		$scope.gid = $routeParams.gid;

		$scope.chatMgsdetails = [{uName: 'Santosh', msg: 'Hello Tina, how are you doing today?', dateTime: "04/06/2018"},
			                     {uName: 'Tina', msg: 'I am doing good, how about yours?', dateTime: "04/06/2018"},
			                     {uName: 'Santosh', msg: 'ya, i am also doing good', dateTime: "04/06/2018"},
			                     {uName: 'Santosh', msg: 'Hello Tina, how are you doing today?', dateTime: "04/06/2018"},
			                     {uName: 'Tina', msg: 'I am doing good, how about yours?', dateTime: "04/06/2018"},
			                     {uName: 'Santosh', msg: 'ya, i am also doing good', dateTime: "04/06/2018"},
			                     {uName: 'Santosh', msg: 'Hello Tina, how are you doing today?', dateTime: "04/06/2018"},
			                     {uName: 'Tina', msg: 'I am doing good, how about yours?', dateTime: "04/06/2018"},
			                     {uName: 'Santosh', msg: 'ya, i am also doing good', dateTime: "04/06/2018"}
		]

		$scope.sendFlag = false;
		$scope.checkLength = function(){
			if($scope.chatText.length > 0) {
				$scope.sendFlag = true;
			}else{
				$scope.sendFlag = false;
			}
		}

		//Get chat history
		chatService.getChatHistory($scope.gid).then(function(){
		}, function(error){
			console.log("API connection error");
		});

		//Post Text
		$scope.postMsg = function() {
			var d = new Date();
			var y = d.getFullYear();
			var m = d.getMonth() + 1;
			var d = d.getDate();

			var dateTime = "";
			dateTime = d+"/"+m+"/"+y;
			$scope.newText = {};
			$scope.newText.uName = "Santosh";
			$scope.newText.msg = $scope.chatText;
			$scope.newText.dateTime = dateTime;
			$scope.chatMgsdetails.push($scope.newText);
			$scope.chatText = "";

			/*if(chatText.length > 0) {
				//$scope.sendFlag = true;
                chatService.postChatHistory($scope.gid, $scope.chatText).then(function () {
                }, function (error) {
                    console.log("API connection error");
                });
            }*/
        }



	});


	chatApp.service("chatService", function($http, $q){
		var df = $q.defer();

		//Get Chat history services
		this.getChatHistory = function(gid) {
            $http({
                method: 'GET',
                url: 'apiUrl',
                param: {gid: gid}
            }).then(function (response) {
                df.resolve();
            }, function (error) {
                df.reject();
            });
            return df.promise;
        }

        //Post text
        this.postChatHistory = function(gid) {
            $http({
                method: 'POST',
                url: 'apiUrl',
                data: {gid: gid}
            }).then(function (response) {
                df.resolve();
            }, function (error) {
                df.reject();
            });
            return df.promise;
        }

	});
